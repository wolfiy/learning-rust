# Chapter 3
## Variables and mutability
By default, variables are *immutable*, meaning the value is bound to the name 
and cannot be changed. For a variable to be declared mutable, the `mut` keyword 
is needed. `let` can only be used in functions.

```rust
fn main() {
    // Without the 'mut' keyword, the code will not compile since an immutable
    // variable cannot be assigned twice.
    let mut x = 5;

    // 'x' has a value of 5.
    println!("The value of x is: {x}");
    
    x = 6;
    
    // 'x' now has a value of '6'.
    println!("The value of x is: {x}");
}
```

### Constants
Cannot be used with `mut` and declared using the `const` keyword.
```rust
const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;
```
They can only be assigned constant expression, and *not* to the result of a 
value that would be computed at runtime. The convention is to use all uppercase 
with underscores between words. `const` can be used in the global scope.

### Shadowing
A new variable with the same name as a previous one can be declared. This can be
useful to alter a variable in a specific scope:
```rust
fn main() {
    let x = 5;
    let x = x + 1;

    {
        let x = x * 2;
        // The value of 'x' is 12.
        println!("The value of x in the inner scope is: {x}");
    }

    // The value of 'x' is 6.
    println!("The value of x is: {x}");
}
```
Since shadowing is effectively creating a new variable, the type can be changed
(unlike with a `mut` variable).
```rust
let spaces = "   ";
let spaces = spaces.len();
```

## Data types
All variable types must be known at compile time!

### Scalar types
Represent a single value.
- Integers (e.g. `i8`, `u16`, `i128`, where `ixxx` is for signed [2's 
complement], `uxxx` for unsigned of *xxx* bits);
- Floats (`f32`, `f64`);
- Booleans (`bool`);
- Characters (`char`, **UTF-8** and specified using single quotes).

### Compound types
Can group multiple values into one type.

#### Tuple type
They have a fixed size.
```rust
let tup: (i32, f64, u8) = (500, 6.4, 1);

let x: (i32, f64, u8) = (500, 6.4, 1);
let five_hundred = x.0;
let six_point_four = x.1;
let one = x.2;
```

#### Arrays
Here all elements must have the same type. Their size is also fixed.
```rust
let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
let a: [i32; 5] = [1, 2, 3, 4, 5];
let b = [3; 5]; // Same as let b = [3, 3, 3, 3, 3];

// Accessing elements
let first = a[0];
let second = a[1];
```

## Functions
A function is declared using the `fn` keyword. `main()` is the entry point of 
the program. In function signatures, the type of each parameter *must* be 
declared.
```rust
// Outputs "The measurement is: 5h".
fn main() {
    print_labeled_measurement(5, 'h');
}

fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {value}{unit_label}");
}

// A function that returns a value of a certain type
fn five() -> i32 {
    5
}

fn main2() {
    let x = five();
    println!("The value of x is: {x}"); // '5'.
}
```

## Comments
Comments - even ones extending to multiple lines - are made with `//`.

## Control flow
Running code depending on whether/while a condition is true.

### If expressions
```rust
fn main() {
    let number = 3;

    // Condition must be a bool!
    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
}
```
For cases with a  lot of `else if`, `match` is a more elegant solution.

Since `if` is an expression, it can be used on the right side of a `let` 
statement like so:
```rust
fn main() {
    let condition = true;
    let number = if condition { 5 } else { 6 };

    println!("The value of number is: {number}"); // 5.
}
```
> Note that types must match.

### Loops
To exit out of a loop, the `break` keyword can be used.
```rust
fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {result}"); // 20.
}
```

Loops can be labeled to help the managing of `break` and `continue`:
```rust
fn main() {
    let mut count = 0;
    'counting_up: loop {
        println!("count = {count}");
        let mut remaining = 10;

        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
            remaining -= 1;
        }

        count += 1;
    }
    println!("End count = {count}");
}
```

#### Conditional loops
While the condition is true, the loop runs.
```rust
fn main() {
    let mut number = 3;

    while number != 0 {
        println!("{number}!");

        number -= 1;
    }

    println!("LIFTOFF!!!");
}
```
For loops can be used to go over a collection.
```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a {
        println!("the value is: {element}");
    }
}
```

```rust
fn main() {
    for number in (1..4).rev() { // 'rev()' reverses the range.
        println!("{number}!");
    }
    println!("LIFTOFF!!!");
}
```