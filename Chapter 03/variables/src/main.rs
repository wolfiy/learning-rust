fn main() {
    // Without the 'mut' keyword, the code will not compile since an immutable
    // variable cannot be assigned twice.
    let mut x = 5;

    // 'x' has a value of 5.
    println!("The value of x is: {x}");
    
    x = 6;
    
    // 'x' now has a value of '6'.
    println!("The value of x is: {x}");


    // A constant is an immutable variable that can only be set to a constant
    // expression, not the result of a value computed at runtime.
    const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;
}
