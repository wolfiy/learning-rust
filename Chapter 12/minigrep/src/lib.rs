use std::error::Error;
use std::fs;

pub struct Config {
    pub query: String,
    pub file_path: String,
}

impl Config {
    /// Returns the query to search for and the path of the file to search in.
    pub fn build(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("Not enough arguments.");
        }

        // A panic macro also works, but is more suited to a programming 
        // problem and not a usage one.
        // if args.len() < 3 {
        //     panic!("Not enough arguments");
        // }

        // 'args[0]' holds the program's name. See the debug line.
        let query = args[1].clone();
        let file_path = args[2].clone();

        Ok(Config { query, file_path })
    }
}

/// Reads a file from passed arguments.
pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.file_path)?;
    
    println!("With text:\n{contents}");

    Ok(())
}