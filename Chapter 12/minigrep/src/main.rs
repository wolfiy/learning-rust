use std::env;
use std::process;

use minigrep::Config;

/// The `minigrep` program searches for a specific string of text in a given
/// file. This piece of software was made while reading the chapter 12 of the
/// Rust programming language book.
fn main() {
    // The 'main()' function should be responsible of:
    // - Calling the command line parsing logic with the arguments;
    // - Set up any other configuration;
    // - Call a 'run()' function in lib.rs;
    // - Handle errors returned by 'run()'.

    // Note. 'env::args()' will panic if any argument contains invalid
    // Unicode. 'args_os' would not but it uses OsStrings instead.
    // 'collect()' turns the iterator into a vector containing all the values.
    let args: Vec<String> = env::args().collect();

    let config = Config::build(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    println!("Searching for {}", config.query);
    println!("In file {}", config.file_path);

    // We only care about detecting an error since 'run()' returns a '()' when 
    // it works as expected.
    if let Err(e) = minigrep::run(config) {
        println!("Application error: {e}");
        process::exit(1);
    }

    // Debug macro used to print the vector.
    // dbg!(args);
}