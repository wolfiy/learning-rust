# Chapter 11
The compiler already takes care of the type checking and borrow checking. 
*Automated tests* are useful to check whether the program does what we intended 
it to do.

## Writing tests
Usually, the bodies of test methods perform the following:

1. Setting up of any needed data or state;
2. Running the code to test;
3. Asserting the resulting output.

When making a library with Cargo, a test module is automatically created. To 
run them, execute `cargo test`. Not that benchmark tests exist to measure 
performence.

```rust
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
```

### Testing results
The `assert!` macro calls `panic!` if false:

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_cannot_hold_larger() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(!smaller.can_hold(&larger));
    }
}
```

### Testing (in)equality
Same principle but with `assert_eq!` and `asser_ne!`.

```rust
pub fn add_two(a: i32) -> i32 {
    a + 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_two() {
        assert_eq!(4, add_two(2));
    }
}
```

> **Note.** In rust, the parameters to equality assertion are called left and 
right, and their order does not matter. The tested types will also need to 
implement `PartialEq` and `Debug`.

### Custom failure message
This can be done using the `format!` macro.

```rust
pub fn greeting(name: &str) -> String {
    format!("Hello {}!", name)
}

#[cfg(test)]
mod tests {
    use super::*;

    // Kind of useless
    #[test]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        assert!(result.contains("Carol"));
    }

    // Better
        #[test]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        assert!(
            result.contains("Carol"),
            "Greeting did not contain name, value was `{}`",
            result
        );
    }
}
```

### Checking panics
Add an attribute `should_panic`.

```rust
pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess { value }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn greater_than_100() {
        Guess::new(200);
    }
}
```

Those tests are not very precise. Add an `expected` parameter to help.

```rust
// --snip--

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 {
            panic!(
                "Guess value must be greater than or equal to 1, got {}.",
                value
            );
        } else if value > 100 {
            panic!(
                "Guess value must be less than or equal to 100, got {}.",
                value
            );
        }

        Guess { value }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "less than or equal to 100")]
    fn greater_than_100() {
        Guess::new(200);
    }
}
```

`Result<T, E>` can also be used in tests:

```rust
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}
```

## Controlling how tests are run
By default, tests are run in parallel using threads. To avoid this, one can run 
`$ cargo test -- --test-threads=1`.

When a test passes, nothing from the standard output will be displayed (so any 
calls to `println!` will be ignored). To force the display of output, run 
`cargo test -- --display-output`.

To run a subset of tests by name: 

```rust
pub fn add_two(a: i32) -> i32 {
    a + 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_two_and_two() {
        assert_eq!(4, add_two(2));
    }

    #[test]
    fn add_three_and_two() {
        assert_eq!(5, add_two(3));
    }

    #[test]
    fn one_hundred() {
        assert_eq!(102, add_two(100));
    }
}
```

```bash
# All tests.
cargo test

# A specific test (one_hundred).
cargo test one_hundred

# A subset of tests (add_two_and_two, add_three_and_two).
cargo test add
```

It is also possible to ignore tests unless they are specifically requested:
```rust
#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}

#[test]
#[ignore]
fn expensive_test() {
    // code that takes an hour to run
}
```

To run only the ignored tests: `cargo test -- --ignored`.

## Test organization
*Unit tests* are small and focused, testing one module in isolation at a time. 
*Integration tests* are entirely external to the library and use the code in 
the same way any other external code would: by using only the public interface. 

### Unit test
Unit tests are put in the *src* directory in each file with the code that they 
are testing. The convention is to create a module named *tests* in each file to 
contain the test functions and annotate the module with `cfg(test)`.

#### Test modules
Using that, the compiler will compile and put the tests in the binary only when 
running `cargo test`.

```rust
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
```

> **Notes.** It is possible to test private functions in Rust.

### Integration tests
The goal is to test whether many parts of the code work together correctly.

```
adder
├── Cargo.lock
├── Cargo.toml
├── src
│   └── lib.rs
└── tests
    └── integration_test.rs
```

```rust
use adder;

#[test]
fn it_adds_two() {
    // Each file in 'test' is a different crate; bring into scope.
    assert_eq!(4, adder::add_two(2));
}
```

We can organize integration tests with multiple files.

```rust
// tests/common/mod.rs
pub fn setup() {
    // setup code specific to your library's tests would go here
}
```

```rust
// tests/integration_test.rs
use adder;

mod common;

#[test]
fn it_adds_two() {
    common::setup();
    assert_eq!(4, adder::add_two(2));
}
```

```
├── Cargo.lock
├── Cargo.toml
├── src
│   └── lib.rs
└── tests
    ├── common
    │   └── mod.rs
    └── integration_test.rs
```