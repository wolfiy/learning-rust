# Chapter 7
To better manage growing project, one should separate and organize the code 
into multiple files, modules, libraries, etc. A *package* contains multiple 
binary files and optionally one library.

> Packages: A Cargo feature that lets you build, test, and share crates

> Crates: A tree of modules that produces a library or executable

> Modules and use: Let you control the organization, scope, and privacy of 
paths.

>Paths: A way of naming an item, such as a struct, function, or module

## Packages and crates
A *crate* is the smallest amount of code the Rust compiler considers at a time. 
It can contain modules which can be defined in other files and get compiled 
alongside the crate. It comes in two forms: binary (compiled to a runnable 
executable) or library (define functionality intended to be shared with 
multiple projects) crate. The *crate root* is a source file used by the Rust 
compiler to make up the root module of the crate.

A *package* is a bundle of one or more crates. It does not have any specific 
number of crates but only one library and at leaste one crate.

### Modules cheat sheet
> **Start from the crate root.** The compiler first looks in the crate root 
file (usually `src/lib.rs`) for a library crate for code to compile.

> **Declaring modules.** In the crate root file, with `mod modulename`.

> **Declaring submodules.** In any file other than the root crate, submodules 
can be declared.

> **Paths to code in modules.** Once a module is part of a crate, the code 
within the module can be referred to from anywhere else in the same crate, as 
long as the privacy rules allow it (e.g. `crate::garden::vegetable::Aspargus`).

> **Private / public.** Code is by default private. Add the keyword `pub` to 
make it public from its parent modules.

> **`use` keyword.** Within a scope, creates shortcuts to items to reduce 
repetitions of long paths.


On an example:
```
backyard
├── Cargo.lock
├── Cargo.toml
└── src
    ├── garden
    │   └── vegetables.rs
    ├── garden.rs
    └── main.rs
```

```rust
use crate::garden::vegetables::Asparagus;

// Tells the compiler to include the code it finds in src/garden.rs
pub mod garden;

fn main() {
    let plant = Asparagus {};
    println!("I'm growing {:?}!", plant);
}
```

```rust
// src/garden.rs
pub mod vegetables;

// Here, pub mod vegetables; means the code in src/garden/vegetables.rs 
// is included too. That code is:

#[derive(Debug)]
pub struct Asparagus {}
```

### Grouping related code in modules
Modules let us organize code within a crate for readability and ease of use. 
They also allow us to control the privacy of items.

Let's take a restaurant for example. First, run `cargo new restaurant --lib`.

```rust
// src/lib.rs
mod front_of_house {
    mod hosting {
        fn add_to_waitlist() {}

        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}

        fn serve_order() {}

        fn take_payment() {}
    }
}
```

Inside modules, we can also place other modules or definitions for other items. 
By using them, we can group related definitions together and name why they are 
related.

```
crate
 └── front_of_house
     ├── hosting
     │   ├── add_to_waitlist
     │   └── seat_at_table
     └── serving
         ├── take_order
         ├── serve_order
         └── take_payment
```

## Paths for referring to an item in the module tree
Two forms:
- Absolute path: starting from a crate root, for code from an external crate;
- Relative path: starts from the current module and uses `self`, `super` or an 
identifier in the current module.

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path.
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path.
    front_of_house::hosting::add_to_waitlist();
}

fn deliver_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        // Relative path using 'super'.
        super::deliver_order();
    }

    fn cook_order() {}
}
```

> In Rust, all items are private by default. Parents cannot use private child 
modules, but children can use their ancestors'. `pub` can be used to make items 
public, but be careful as it does *not* make the content public.

### Making enums and structs public
Adding `pub` before a struct makes it public, but its fields remain private. 
Each field has to be set to public on a case by case basis.

```rust
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }
}

pub fn eat_at_restaurant() {
    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");
}
```

Enums however are fully public .

## Bringing paths into scope with the use keyword
Similar to creating a symlink in the filesystem. Be careful as these only work 
in the scope they were defined.

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

use crate::front_of_house::hosting;

// Works fine.
pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}

// Does not compile.
mod customer {
    pub fn eat_at_restaurant() {
        hosting::add_to_waitlist();
    }
}
```

If two items with the same name would be brought into scope:
```rust
use std::fmt;
use std::io;

fn function1() -> fmt::Result {
    // --snip--
}

fn function2() -> io::Result<()> {
    // --snip--
}
```

```rust
use std::fmt::Result;
use std::io::Result as IoResult;

fn function1() -> Result {
    // --snip--
}

fn function2() -> IoResult<()> {
    // --snip--
}
```

### Using external packages
Adding a crate as a dependency in the `Cargo.toml` file tells Cargo to download 
the required files from Crate.io. To then bring such crate into scope, use 
`use`.

```rust
use rand::Rng;
// Bringing multiple items into scope.
use std::{cmp::Ordering, io};
use std::io::{self, Write};
use std::collections::*;

fn main() {
    let secret_number = rand::thread_rng().gen_range(1..=100);
}
```

## Separating modules into different files
On the restaurent example, we can move the moduke definitions to a separate 
file to make the code easier to navigate. We first extract the `front_of_house` 
module into it own file, leaving only the `mod font_of_house` declaration.

```rust
// src/lib.rs
mod front_of_house;

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}
```
```rust
// src/front_of_house.rs
pub mod hosting;
```
```rust
// src/front_of_house/hosting.rs
pub fn add_to_waitlist() {}
```

> It is only needed to load a file using a `mod` declaration once in the module 
tree. *Also, not that `use` is not a `include` operation.*