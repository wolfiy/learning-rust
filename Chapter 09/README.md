# Chapter 9
In many cases, Rust requires the developer to acknowledge the possibility of 
an error and take some action before compiling. This allows for more robust 
code. Rust groups errors into two categories:

1. Recoverable (will be reported to the user and the operation will be tried 
once again);
2. Unrecoverable (symptoms of bugs and immediatly stop the program).

Rust does not have exceptions, instead it uses the type `Result<T, E>` and the 
`panic!` macro.

## Unrecoverable errors with `panic!`
`panic!` can be called either by taking an illegal action (such as accessing an 
array past the end) or by explicitely calling the macro. Eitherway, these 
panics will print a failure message, unwind, clean up the stack and quit.

> By default, the program starts *unwinding* when a panic occurs. This means 
Rust walks back up the stack and cleans up the data from each function it 
encounters. Since it is a lot of work, it can also be skipped. This can be done 
by adding `panic = 'abort'` to the `[profile]` section of the *Cargo.toml* file.

### Using a `panic!` backtrace
To get a backtrace, run `RUST_BACKTRACE=1 cargo run`

## Recoverable errors with result
Most errors are not serious enough to require the program to stop entierly (for 
instance if it tries to open an inexistant file, it would be better to just 
create it).

The result enum is defined as follow, with an expected value of type `T` and an 
error of type `E`.

```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

Say we want to open a file:

```rust
use std::fs::File;

fn main() {
    // 'T' is a file handle, 'E' is std::io::Error.
    let greeting_file_result = File::open("hello.txt");
}

// If we wanted to take different actions depending on the value of File::open.
fn main() {
    let greeting_file_result = File::open("hello.txt");

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
}
```

When the result is `Ok`, the code will return the inner file value out of the 
`Ok` variant, and we then assign that file handle to a variable. After the 
match, we can use the file handle for reading or writing. The other arm handles 
the case of an error.

### Matching different errors
We might want to take different actions for different failure reasons.

```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let greeting_file_result = File::open("hello.txt");

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error);
            }
        },
    };
}
```

Closures can also be used for a cleaner, shorter code:

```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let greeting_file = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
}
```

### Shortcuts for panic on error: unwrap and expect
`unwrap` will return the value inside the `Ok` variant or call the `panic!` 
macro. 

```rust
use std::fs::File;

fn main() {
    // If no file is found, an error will be called.
    let greeting_file = File::open("hello.txt").unwrap();
}

// To choose the error message, use 'expect()'. Better practice.
fn main() {
    let greeting_file = File::open("hello.txt")
        .expect("hello.txt should be included in this project");
}
```

### Propagating errors
It is also possible to return the error to the calling code

```rust
use std::fs::File;
use std::io::{self, Read};

// If everything works as intended, the calling function will receive an 'Ok' 
// value containing the username; otherwise an error.
fn read_username_from_file() -> Result<String, io::Error> {

    // Tries to read the username...
    let username_file_result = File::open("hello.txt");

    // ...if it fails, return an error instead of panicking.
    let mut username_file = match username_file_result {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    // If there is a username, store it in here.
    let mut username = String::new();

    // Tries to return the username from the above string.
    match username_file.read_to_string(&mut username) {
        Ok(_) => Ok(username),
        Err(e) => Err(e),
    }
}
```

The code that calls the above code will have to handle getting either an `Ok` 
value or an `Err` one. It is up to it to decide what to do. This pattern of 
*propagating* errors is so common that Rust provides the `?` operator to make 
this easier.

```rust
// Equivalent to the code above.
use std::fs::File;
use std::io::{self, Read};

fn read_username_from_file() -> Result<String, io::Error> {
    // Works like a 'match' expression.
    let mut username_file = File::open("hello.txt")?;
    let mut username = String::new();
    username_file.read_to_string(&mut username)?;
    Ok(username)
}
```

The difference between a `match` and `?` is that the latter's errors go through 
the `from` function, defined in the `From` trait, which converts the error type 
needed for the return statement.

The code could even be made shorter:

```rust
fn read_username_from_file() -> Result<String, io::Error> {
    let mut username = String::new();

    File::open("hello.txt")?.read_to_string(&mut username)?;

    Ok(username)
}
```

```rust
use std::fs;
use std::io;

// Since what was done above is fairly common, there exists a shorter way
// accessible through the standard library.
fn read_username_from_file() -> Result<String, io::Error> {
    fs::read_to_string("hello.txt")
}
```

> **Note.** The `?` operator can only be used in functions whose return type is 
compatible with the value the `?` is used on, because it is defined to perform 
an early return of a value out of the function. 

```rust
use std::fs::File;

// Does not compile: types do not match.
fn main() {
    let greeting_file = File::open("hello.txt")?;
}

// Does compile. 'Box<dyn Error>' can be understood as "any error".
fn main() -> Result<(), Box<dyn Error>> {
    let greeting_file = File::open("hello.txt")?;

    Ok(())
}
```

## To panic! or not to panic!
- Returning `Result` is a good default choice when defining a function that 
might fail;
- When prototyping, testing and making examples, panics are more useful.

### Cases in which you have more information than the compiler
The compiler does not understand logic; there will always be a `Result` value 
to handle. If you can ensure by manually inspecting the code that you will 
never have an `Err` variant, it is acceptable to call `unwrap()` and even 
better to document the reason.

```rust
use std::net::IpAddr;

// The compiler is not smart enough to understand that no errors will happen.
// We still have to handle the 'Result' as if an 'Err' was a possiility.
let home: IpAddr = "127.0.0.1".parse()
                              .expect("Hardcoded IP address should be valid");
```

### Guidelines
It is advisable to have the code panic when there is a possibility of ending up 
in a bad state (vroken invariant, contradictions, missing values, ...) plus any 
of the following:

- The bad state is unexpected (**not** something like wrong date format, etc.);
- The code needs to rely on not being in a bad state further down the line;
- There is no good way to encode this information in the types used (cf. 17).

If the code is called with values that do not make sense, it is better to 
return an error. In cases where continuing could be insecure or harmful, it is 
better to call `panic!` (do not foget to explain why). `panic!` is also often 
appropriate for calls to external libraries.

When failure is expected (parser with malformed data, HTTP hit rate limit, 
etc.), return a `Result`. If performing an operation with invalid values could 
put the user at risk, use `panic!` (e.g. vulnerabilities, overflows, etc.).

### Creating custom types for validation
One way of checking for valid values is to use `if` statements:

```rust
loop {
    // --snip--

    let guess: i32 = match guess.trim().parse() {
        Ok(num) => num,
        Err(_) => continue,
    };

    if guess < 1 || guess > 100 {
        println!("The secret number will be between 1 and 100.");
        continue;
    }

    match guess.cmp(&secret_number) {
        // --snip--
    }
}
```

However, with more requirements this can become tedious and expensive. Instead, 
we can make a new type and put the validation in a function to create an 
instance of that new type rather than repeating validations. This ensures for 
valid input parameters.

```rust
pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess { value }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}
```