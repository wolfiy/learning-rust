# Chapter 6
Enumerations (`enum`) allowsthe definition of a type by enumerating its 
possible variants.

## Defining an enum
Structs give a way of grouping related fiedls and data together, and enums give 
a way of saying a value is one of a possible set of values (such as recangles 
among shapes).

```rust
// Definition

enum IpAddrKin {
    V4,
    V6,
}
```

### Enum values
An instance of each variant can be created like this:

```rust
// Both values are of the same type...
let four = IpAddrKind::V4;
let six = IpAddrKind::V6;

// ...so a function like this one would work just fine.
fn route(ip_kind: IpAddrKind) {}

fn main() {
    route(IpAddrKind::V4);
    route(IpAddrKind::V6);
}
```

```rust
// It is also possible to store  data into each enum variant, which removes the 
// need for an extra struct.
enum IpAddr {
        V4(String),
        V6(String),
    }

    let home = IpAddr::V4(String::from("127.0.0.1"));
    let loopback = IpAddr::V6(String::from("::1"));
```

Enums also allow for different types on different variants:
```rust
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String)
}

let home = IpAddr::(127, 0, 0, 1);
let loopback = IpAddr::V6(String::from("::1"));
```

Another implementation is by using the `IpAddr` library:
```rust
struct Ipv4Addr {
    // Code...
}

struct Ipv6Addr {
    // Code...
}

enum IpAddr {
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}
```
Another example of enum using different types:
```rust
// Four different types are being used:
// - 'Quit' has no data associated with it at all;
// - 'Move' has named fields, just like a struct;
// - 'Write' includes a single String;
// 'ChangeColor' includes three 'i32' values.
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

// Which is equivalent to:
struct QuitMessage;                         // Unit struct
struct MoveMessage {
    x: i32;
    y: i32;
}
struct: WriteMessage(String);               // Tuple struct
struct ChangeColorMessage(i32, i32, i32);   // Tuple struct
```

The latter implementation don't easily allow to define a function to take any 
of these kind of messages, unlike the former.

Just like with structs, we can define methods using `impl`. For instance:

```rust
impl Message {
    fn call(&self) {
        // Code to get the value we called the method on.
    }
}

let m = Message::Write(String::from("hello"));
m.call();   // "hello".
```

### The openim enum and its advantages over null values
`Option` is another enum defined in the standard library that encodes the 
scenario in which a value could be something or nothing.

> **Note.** Rust does not have the null feature.

#### Encoding absent values
There exists an enum that can encode the concept of a value being present or 
not: `Option<T>`. Its definition is:

```rust
enum Option<T> {
    None,
    Some(T),
}
```
It does not need to be brought into scope (i.e. no need for `::`). It works 
just like any other enum. The `<T>` syntax represent a generic type. In 
practice, it can be any type.

```rust
let some_number = Some(5);              // Type is 'Option<i32>'.
let some_char = Some('e');              // Type is 'Option<char>'.
let absent_number: Option<i32> = None;  // Type is 'Option<i32>'.
```

This is better than having null because `Option<T>` and `T` are different 
types, and the compiler won't let us use `Option<T>` as if it were a valid 
value.

Before performing `T` operations on an `Option<T>`, it has to be converted to 
a `T`, which prevents us from assuming something null non null (check 
documentation).

## The match control flow construct
Allows the comparison of a value against a series of patterns and execute code 
accordingly. Patterns can be made of literal values, variable names, wildcards, 
etc.

```rust
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

// Arms are executed in order.
fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}
```

### Patterns that bind to values
Match arms can also bind to the parts of the values that match the pattern 
(like values of enum variants). On the previous example, we can add variants of 

```rust
#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}
```

### Matching with Option<T>
In the previous example we wanted to get the inner `T` value out of the `Some` 
case when using `Option<T>`. We could have also used a match expression:

```rust
// Takes an 'Option<i32>' and adds 1 if there is a value.
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

let five = Some(5);
let six = plus_one(five);   // 6
let none = plus_one(None);  // None
```

> **Note.** The arms' patterns must cover all possibilities, otherwise the code 
will not compile.

### Catch-all patterns and the '_' placeholder
On the following example, the code compile anyway since the last arm takes 
care of all unspecified values:

```rust
let dice_roll = 9;
match dice_roll {
    3 => add_fancy_hat(),
    7 => remove_fancy_hat(),
    other => move_player(other),
}

fn add_fancy_hat() {}
fn remove_fancy_hat() {}
fn move_player(num_spaces: u8) {}
```

Rust also has a pattern we can use as a catch-all when we don't want to use the 
calues in the pattern: `_`. This tells Rust the value is not going to be used, 
thus we will not be warned about unused variable.

```rust
let dice_roll = 9;
match dice_roll {
    3 => add_fancy_hat(),
    7 => remove_fancy_hat(),
    _ => reroll(),              // Or '_ => ()' for nothing to happen
}

fn add_fancy_hat() {}
fn remove_fancy_hat() {}
fn reroll() {}
```

## Concise control flow with if let
The `if let` syntax allows for a less verbose way of handling values that match 
a pattern while ignoring the rest.

```rust
let config_max = Some(3u8);
match config_max {
    Some(max) => println!("The maximum is configured to be {}", max),
    _ => (),
}

// Is equivalent to
let config_max = Some(3u8);
if let Some(max) = config_max {
    println!("The maximum is configured to be {}", max);
}
```

Else if are also possible:

```rust
let mut count = 0;
match coin {
    Coin::Quarter(state) => println!("State quarter from {:?}!", state),
    _ => count += 1,
}

// Is equivalent to
let mut count = 0;
if let Coin::Quarter(state) = coin {
    println!("State quarter from {:?}!", state);
} else {
    count += 1;
}
```