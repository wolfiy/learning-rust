# Chapter 8
Collection can contain multiple values. Unlike tuples and array, data these 
collections point to is stored on the heap. We have:

- **Vectors:** stores a variable amount of values next to eachothers;
- **Strings:** collection of characters;
- **Hash maps:** associate values with keys.

## Storing lists of values with vectors
`Vec<T>` allows storage of more than one value (of the same type) in a single 
data structure that puts all the values next to each other in memory.

### Creating vectors
A new vector can be created like so:

```rust
// Empty vector.
let v: Vec<i32> = Vec::new();

// With initial values
let v = vec![1, 2, 3];
```

### Updating and reading vectors
```rust
let mut v = Vec::new();

// Adding values to the vector.
v.push(5);
v.push(6);
v.push(7);
v.push(8);
```

### Reading elements of vectors
Both indexing and the `get` method can be used.

```rust
let v = vec![1, 2, 3, 4, 5];

// Getting a reference.
let third: &i32 = &v[2];
println!("The third element is {third}");

// 'get' returns a reference.
let third: Option<&i32> = v.get(2);
match third {
    Some(third) => println!("The third element is {third}"),
    None => println!("There is no third element."),
}

// Panics.
let does_not_exist = &v[100];

// Returns 'none'.
let does_not_exist = v.get(100);
```

Rules of ownership and borrowing still apply:

```rust
// Does not compile.
let mut v = vec![1, 2, 3, 4, 5];
let first = &v[0];
v.push(6);
println!("The first element is: {first}");
```

### Iterating over the values in a vector
It is possible to iterate over all elements of a vector instead of using 
indexes. It is a safe operation thanks to the borrow checkers' rules.

```rust
// Using a loop to get immutable references to each elements.
let v = vec![100, 32, 57];
for i in &v {
    println!("{i}");
}

// Looping over mutable references to each element in a mutable vector to 
// change their value. The '*' dereference operator is used to get the value in 
// 'i' befaore changing it.
let mut v = vec![100, 32, 57];
for i in &mut v {
    *i += 50;
}
```

### Using an enum to store multiple types
When we need one type to represent elements of different types, we can define 
and use an enum. Let's look at an example: a spreadsheet with columns 
containing integers, floating point numbers and strings.

```rust
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

let row = vec![
    SpreadsheetCell::Int(3),
    SpreadsheetCell::Text(String::from("blue")),
    SpreadsheetCell::Float(10.12),
];
```

Rust needs to know the types will be in the vector at compile time to know 
exactly how much memory on the heap will be needed. Using an enum with a match 
expression means Rust will ensure at compile time that every possible case has 
been handled. If the exhaustive set of types gotten at runtime is unknown, the 
enum technique will not work. Instead, a `trait` object can be used.

### Dropping a vector drops its elements
Like other structures, a vector is freed when it goes out of scope.

```rust
{
    let v = vec![1, 2, 3, 4];

    // do stuff with v
} // <- v goes out of scope and is freed here
```

When it gets dropped, all of its contents are also dropped.

## Storing UTF-8 encoded text with strings
In Rust, Strings are implemented as a collection of bytes, with some 
additionnal methods to provide useful functionality when those are interpreted 
as text.

### Definition of a string
Rust only has string type in the core language: the string slice `str`. String 
literals, for instance, are stored in the program's binary and are therefore 
string slices. The `String` type is a growable, mutable, owned, UTF-8 encoded 
string type. 

### Creation
Many of `Vec<T>`'s operations are still available with `String` since it 
actually is implemented as a wrapper around a vector of bytes with some extra 
guarantees, restrictions and capabilities.

```rust
// Creation of a new empty 'String'.
let mut s = String::new();

let data = "initial contents";
let s = data.to_string();

// Would have also worked.
let s = "initial contents".to_string();
let s = String::from("initial contents");
```

> **Note.** Strings are UTF-8 encoded, meaning any properly encoded data can be 
included (i.e. non latin characters work too).

### Updating
A `String` can grow in size and its contents can change.

```rust
// Will concatenate both Strings.
let mut s = String::from("foo");
s.push_str("bar");                  // Takes a string slice (don't want to own).

// We want to be able to use 's2' afterwards.
let mut s1 = String::from("foo");
let s2 = "bar";
s1.push_str(s2);
println!("s2 is {s2}");
```

The `push` method adds a single character.

```rust
let mut s = String::from("lo");
s.push('l');
```
    
Strings can also be combined using the `+` operator or `format!` macro:

```rust
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
```

When using `+`, the `add` method is called. The reason `s1` becomdes invalid 
has to do with its signature: `fn add(self, s:&str) -> String {}`. 

First, `s2` has an `&`, meaning that we are adding a reference of the second 
string to the first (because of the `s` parameter of `add`). Here, we were able
to use a `&String` because the compiler can coerce the argument into a `&str`. 
Since `add` does not take ownership of the `s` parameter, `s2` remains valid.

Second, we can see in the signature that `add` takes onership of `self` (it 
does not have an `&`), meaning `s1` will be moved into `add` and no longer be 
valid. So although `s3 = s1 + &s2;` looks like it will copy both strings to 
create a new one, it actually takes ownership of `s1`, appends a copy of `s2`'s 
contents and returns ownership of the result. This implementation is more 
efficient than copying.

```rust
// When concatening multiple strings the behavior of '+' gets unwieldy:
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");
let s = s1 + "-" + &s2 + "-" + &s3;

// Instead, we can use the 'format!' macro:
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");
let s = format!("{s1}-{s2}-{s3}");
```

### Indexing onto strings
Indexing is not supported like with arrays or other languages.

#### Internal representation
A `String` is a wrapper over a `Vec<u8>`. If we take the following `String`:

```rust
let hello = String::from("Hola");
```

`len` will be 4, meaning the vector storing the string is 4 bytes long (each 
letter takes 1 byte in UTF-8). In the case of the following however, the length 
is 24, and *not* 12 as one might expect:

```rust
let hello = String::from("Здравствуйте");
```

This is because each unicode scalar value in that string takes two bytes of 
storage. An index would not corelate with the correct letter.

#### Bytes and scalar values and grapheme clusters
Those are the three relevant ways to look at strings from Rust's perspective. 
If we take the Hindi word "नमस्ते", it is stored as a vector of `u8` values that 
look like this:

```
[224, 164, 168, 224, 164, 174, 224, 164, 184, 224, 165, 141, 224, 164, 164,
224, 165, 135]
```

Which is 18 bytes. If we look at them as unicode scalar values (what Rust's 
`char` type is), it would look like this:

```
['न', 'म', 'स', '्', 'त', 'े']
```

Which is 6 `char` calues, but the 4th and 6th are not letters: they are 
diacritics that do not make sense on their own. If we look at them as grapheme 
clusters, we would get what we could call the four Hindi letters that make up 
the word:

```
["न", "म", "स्", "ते"]
```

All of that allows for more flexibility. Also, indexing operations are expected 
to always be O(1), but it is impossible to guarantee that performance with a 
`String` since Rust would have to walk through the contents from the beginning 
to the index to determine how many valid characters there are.

### Slicing strings
If indices are *really* necessary to create string slices, we have to be more 
specific; rather than indexing using `[]` with a single number, a range can be 
used to create a slice containing particular bytes.

```rust
let hello = "Здравствуйте";
let s = &hello[0..4];   // Will contain the first 4 bytes of the string (Зд).
```

Trying to slice only parts of a character would cause Rust to panic at runtime.

### Iterating over strings
The best way to operate on pieces of strings is to be explicit (characters or 
bytes?). For individual scalar values, the `chars` method can be used.

```rust
// Will print З then д.
for c in "Зд".chars() {
    println!("{c}");
}

// Will print each bytes: 208, 151, 208, 180.
for b in "Зд".bytes() {
    println!("{b}");
}
```

Getting grapheme clusters is complex; it is not provided by the standard 
library.

## Storing keys with associated values in hash maps
The type `HashMap<K, V>` stores a mapping of keys of type `K` to values of type 
`v` using a hashing function. These are useful when you want to look up data 
without using an index. They also store their data on the heap. All keys must 
have the same type as each other, and all of the values must have the same type.

### Creation
On an example, where we are keeping track of the scores of two teams:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

// Adding values
scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

// Accessing values
let team_name = String::from("Blue");
let score = scores.get(&team_name).copied().unwrap_or(0);
```

The `get` method returns an `Option<&V>` (or none if no value match that key). 
This piece of code handles the `Option` by calling `copied` to get an 
`Option<i32>` instead of an `Option<&i32>`, then `unwrap_or` no set scores to 
zero if the scores does not have an entry for the key.

We can iterate over each pair like we can with vectors:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

for (key, value) in &scores {
    println!("{key}: {value}");
}
```

### Hash maps and ownership
For types that implement the `Copy` trait, the values are copied into the hash 
map. Owned values are moved such that the hash map becomes their owner.

```rust
use std::collections::HashMap;

let field_name = String::from("Favorite color");
let field_value = String::from("Blue");

let mut map = HashMap::new();
map.insert(field_name, field_value);
// field_name and field_value are invalid at this point, they cannot be used.
```

Note that if we insert references to values into the hash map, the values will 
not get moved but it is required that they remain valid for the entirety of the 
hash map's lifespan.

### Updating the hash map
Each unique key can only have one value associated with at a time, but not vice 
versa. 

#### Overwriting a value
To replace the values associated with a key.

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Blue"), 25);

println!("{:?}", scores);   // 25.
```

#### Adding a key and a value only if a key is not present
Hash maps have a special API for this called `entry` that take a key as a 
parameter. It returns an enum called `Entry` that represents a value that might 
or might not exist.

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();
scores.insert(String::from("Blue"), 10);

// Insert 50 if the key does not exist.
// 'or_insert' returns a mutable reference to the value for the corresponding 
// 'Entry' key if it exists. If not, it inserts the parameter as the new value 
// for this key and returns a mutable reference to the new value.
scores.entry(String::from("Yellow")).or_insert(50);
scores.entry(String::from("Blue")).or_insert(50);

println!("{:?}", scores);
```

#### Updating a value based on the old value
To, for instance, count how many times each word appears in some text. 

```rust
use std::collections::HashMap;

let text = "hello world wonderful world";

let mut map = HashMap::new();

// 'split_whitespace()' returns an iterator over sub-slices, separated by 
// whitespace.
for word in text.split_whitespace() {
    let count = map.entry(word).or_insert(0);
    *count += 1;
}

println!("{:?}", map);
```

### Hashing functions
By default, `HashMap()` uses `SipHash` which can provide resistance to denial 
of service attacks without involving hash tables. Even though this is not the 
fastest hashing algorithm out there, the increased security is worth it. A 
different hasher can be used using `BuildHasher`.