# Learning Rust

This repository is where I put my notes and all the things I write in my learning of the Rust programming language. I am following the book [The Rust Programming Language](https://rust-book.cs.brown.edu/).
