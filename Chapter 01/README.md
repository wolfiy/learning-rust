# Chapter 1

## Basics

Here are the very basics of Rust.

### Writing and running a Rust program

Rust source files end with the `.rs` file extension. Should the filename consist of more than one word, the convention is to separate them using underscores. To compile a Rust program, run `rustc file.rs`.

### Anatomy of a Rust program

- The first code to be run in every Rust program is the `main` function;

- Indent is done with four spaces;

- A `!` means a Rust macro is called;

- Lines end with a semicolon.

## Cargo

Cargo is Rust's build system and package manager.

### Projects with Cargo

To create a project using Cargo:

```shell
cargo new hello_world
cd hello_world
```

This will create a project with a `Cargo.toml` file containing used packages, dependencies and configuration information needed to compile the program and a `src` folder in which the source files go.

### Compiling Cargo projects

To compile a project made with Cargo, use `cargo build` from the top-level project directory. The default build is a debug build, and the binary will be put inside of the `debug` folder.
The command `cargo run` subsequentially builds and runs the program. `cargo check` can be used to check whether the code will compile, but will not create an executable.
To compile with optimizations (i.e. for release), run `cargo build --release`.
