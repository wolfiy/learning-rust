# Chapter 2

Small guessing game project. Lots of notes inside of the `main.rs` file.

## Variables

A variable is declared by using the `let` statement. `mut` is used to make it mutable (i.e. allow it to change).

```rust
let apples = 5; // Immutable
let mut bananas = 5; // Mutable
```

References, denoted by a `&`, are immutable by default. To print variables, use curly brackets:

```rust
let x = 5;
let y = 10;

println!("x = {} and y = {}", x, y);
```

## Associated functions

A function that is implemented on a type. For instance

```rust
let mut guess = String::new();
```

calls the `new` function from the `String` type, which creates a new empty string.

## Crates

A crate is a collection of Rust source code files. To use an external crate, it must be included in the `Cargo.toml` file first. The version used will be the one specified, which will be saved in the `Cargo.lock` file. When compiling, only changes will be taken into account; if only  the`main.rs` is modified, the dependencies will not be downloaded again nor will they be compiled again.

### Updating a crate

One can ignore and overwrite the `Cargo.lock` file by using `cargo update`. 
