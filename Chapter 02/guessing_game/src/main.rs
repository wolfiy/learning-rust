use std::cmp::Ordering;
use std::io;
use rand::Rng;

/// Entry point  of the program.
fn main() {
    // Generates a random number.
    let secret_number = rand::thread_rng()
                              .gen_range(1..=100);

    // Prints directions to the screen.
    println!("Guess the number!");

    loop {
        println!("Please input your guess: ");

        // A variable that will store the user input.
        let mut guess = String::new();
        
        // Take the user input and append it to the 'guess' variable.
        // The .expect() is used for error handling.
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line.");

        // Converting the 'guess' variable to an unsigned 32-bit number.
        // 'trim()' is used to remove the newline characters and 'parse()'
        // does the conversion.
        let guess: u32 = match guess.trim().parse() {
            // Since 'parse()' returns a 'Result' enum that can be either 'Ok'
            // or 'Err', we can use a match expression to handle errors. '_' is
            // a catchall value.
            Ok(num) => num,
            Err(_) => continue,
        };

        // Prints the 'guess' variable.
        println!("You guessed: {guess}");

        // Compares the two values.
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
