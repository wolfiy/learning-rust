# Chapter 10
*Generics* are abstract stand-ins for concrete types or other properties. We 
can express the behavior and relations of generics without knowing what will 
be in their place when compiling and running.

## Removing duplication by extracting a function
Generics allow us to use placeholder types that represent multiple types to 
avoid duplication of code. The principle is the same as the following, which 
is not using generics:

```rust
// Original code.
fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let mut largest = &number_list[0];

    for number in &number_list {
        if number > largest {
            largest = number;
        }
    }

    println!("The largest number is {}", largest);
}
```

```rust
// Code that would work on n lists.
fn largest(list: &[i32]) -> &i32 {
    let mut largest = &list[0];

    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest(&number_list);
    println!("The largest number is {}", result);

    let number_list = vec![102, 34, 6000, 89, 54, 2, 43, 8];

    let result = largest(&number_list);
    println!("The largest number is {}", result);
}
```

## Generic data types
Generics are used to create definitions for items like function signatures or 
structs, which can later be used with many data types.

### Generic function
The generics are placed in the signature like would concrete types be. This 
makes the code more flexible and prevents duplications. To parametrize the 
types, any identifier can be used but `T` is the convention.

```rust
fn largest<T>(list: &[T]) -> &T { ... }
```

### Generic struct
The syntax is similar to that used in functions. On this example, we have only 
used one generic type, so `x` and `y` must be of the same type.

```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.0, y: 4.0 };
}
```

To define a generic struct with different types:

```rust
struct Point<T, U> {
    x: T,
    y: U,
}

fn main() {
    let both_integer = Point { x: 5, y: 10 };
    let both_float = Point { x: 1.0, y: 4.0 };
    let integer_and_float = Point { x: 5, y: 4.0 };
}
```

### Generic enum
The same principle applies:

```rust
// Useful for optional values.
enum Option<T> {
    Some(T),
    None,
}

// Useful for operations that might fail.
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

### Generic methods
```rust
struct Point<T> {
    x: T,
    y: T,
}

// 'T' has to be declared just after 'impl' to specify that we are implementing 
// methods on the generic type 'Point<T>'.
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

fn main() {
    let p = Point { x: 5, y: 10 };

    println!("p.x = {}", p.x());
}
```

```rust
// To define the method on only one type.
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}
```

Generic type parameters in a struct definition are not always the same as the 
ones used in the struct's method:

```rust
struct Point<X1, Y1> {
    x: X1,
    y: Y1,
}

impl<X1, Y1> Point<X1, Y1> {
    fn mixup<X2, Y2>(self, other: Point<X2, Y2>) -> Point<X1, Y2> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

fn main() {
    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "Hello", y: 'c' };

    let p3 = p1.mixup(p2);

    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}
```

### Performance of generics
Using generics has no impact on runtime performance thanks to monomorphization 
(i.e. it turns all the generic types into concrete one upon compiling).

## Traits: defining shared behavior
A *trait* defines functionality a particular type has and can share with other 
types. This can be used to defined shared behavior in an abstract way. Traits 
bounds can be used to specify that a generic type can be any type that has 
certain behaviors. (Similar to interfaces in Java).

### Definition
A type's behavior consists of the methods we can call on that type. Trait 
definitions are a way to group signatures together to define a set of 
behaviors.

If we had multiple struct representing different types of text, we could define 
the following trait:

```rust
pub trait Summary {
    fn summarize(&self) -> String;
}
```

### Implementation
Continuing on the text example, we cna implement the trait like this:

```rust
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}
```

With that being done, it is now possible to call the trait methods on instances 
of `NewsArticle` and `Tweet`:

```rust
fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());
}
```

> **Note.** We can implement a trait on a type only if at least one of the 
trait or the type i local to our crate.

### Default implementation
Useful to get a default behavior for some or all methods in a trait. As we 
implement the traits on particular types, the default behavior can be 
overridden.

```rust
pub trait Summary {
    fn summarize(&self) -> String {
        String::from("(Read more...)")
    }
}
```

```rust
use aggregator::{self, NewsArticle, Summary};

fn main() {
    let article = NewsArticle {
        headline: String::from("Penguins win the Stanley Cup Championship!"),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from(
            "The Pittsburgh Penguins once again are the best \
             hockey team in the NHL.",
        ),
    };

    // Using the default behavior.
    println!("New article available! {}", article.summarize());
}
```

Default implementations can call other methods in the same trait, even if those 
ones do not have a default implementation:

```rust
// We only need to define summarize_author when implementing the trait.
pub trait Summary {
    fn summarize_author(&self) -> String;

    fn summarize(&self) -> String {
        format!("(Read more from {}...)", self.summarize_author())
    }
}

impl Summary for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }
}
```

Now `summarize` can be called on instances of the `Tweet` struct:

```rust
use aggregator::{self, Summary, Tweet};

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());
}
```

> **Note.** It is not possible to call the default implementation from an 
overriding implementation of the same method.

### Traits as parameters
To define functions that accept many different types using traits:

```rust
// Will accept any type which implements the 'Summary' trait.
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```

#### Trait bound syntax
The `impl` syntax works for straightforward cases but is actually syntax sugar 
for the following:

```rust
pub fn notify<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize());
}

// Two parameters of different types
pub fn notify(item1: &impl Summary, item2: &impl Summary) { ... }

// Two parameters of the same type
pub fn notify<T: Summary>(item1: &T, item2: &T) { ... }
```

### Multiple trait bounds with +
More than one trait boud can be specified:

```rust
pub fn notify(item: &(impl Summary + Display)) { ... }

pub fn notify<T: Summary + Display>(item: &T) { ... }
```

#### Clearer trait bounds with where clauses
Useful to make signatures more readable. The following snippets are equivalent:

```rust
fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 { }

fn some_function<T, U>(t: &T, u: &U) -> i32
where
    T: Display + Clone,
    U: Clone + Debug,
{ }

```

### Returning types that implement traits
The `impl` trait syntax can also be used in the return position in order to 
return a value os some type that implements a trait.

```rust
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    }
}
```

Here, the function returns some type that implements the `Summary` trait 
without naming the concrete type. In this case, it returns a `Tweet` but the 
calling code does not need to know that. This is especially useful for 
iterators and closures.

> **Note.** Works only if a single type is returned.

### Using trait bounds to conditionally implement methods
Methods can be conditionally implemented for types that implement specific 
traits using an `impl` block:

```rust
use std::fmt::Display;

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}
```

## Validating references with lifetimes
*Lifetimes* are a generic that ensure that references are valid as long as we 
need them to be. References always have an implied lifetime, but we must 
annotate lifetimes that could be related in different ways.

### Preventing dangling references with lifetimes
The main goal of lifetimes is to prevent dangling references which cause a 
program to reference data other than the data it's intended to reference.

### The borrow checker
The rust compiler has a borrow checker that compares scopes to determine 
whether all borrows are valid.

### Generic lifetimes in functions
For situations such as returning a borrowed values which might be from a `if` 
or an `else` (i.e. the borrow checker does not know from which parameter the 
value is borrowed).

### Lifetime annotation syntax
These do not change how long any of the references live, but they do describe 
the relationship of the lifetimes ot multiple references to each others.

```rust
&i32        // a reference
&'a i32     // a reference with an explicit lifetime
&'a mut i32 // a mutable reference with an explicit lifetime
```

### Lifetime annotations in function signatures
The returned reference will be valid as long as the parameters are valid:

```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

### Thinking in terms of lifetimes
The way in which we need to specify lifetime parameters depends on what the 
function is doing. When returning a reference from a function, the lifetime 
parameter for the return needs to match the lifetime parameter for one of 
the parameters or a vlue created in the function (but would be a dangling 
reference).

### In struct definitions
We can define structs to hold references, but this requires the adding of a 
lifetime annotation on every reference in the struct's definition.

```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn main() {
    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let i = ImportantExcerpt {
        part: first_sentence,
    };
}
```

### Lifetime elision // TODO re-read
Some predictable and deterministic patterns were added to the compiler to infer 
lifetime in some situations. These *rules of lifetime elision* are not for the 
programmer to follow; they are a set of cases considered by the compiler. If 
some ambiguity remains after applying the rules, Rust will throw an error and 
annotations will have to be added. The rules are:

1. The compiler assigns a lifetime parameter to each parameter that is a 
reference;
2. If there is exactly one input lifetime parameter, that lifetime is assigned 
to all output lifetime parameters;
3. If there are multiple input lifetime parameters but one of theme is `&self` 
or `&mut self`, the lifetime of self is assigned to all output lifetime 
parameters.

### Lifetime annotations in method definitions
On a method whose only parameter is a reference to `self` and whose return 
value is an `i32`, which is not a reference to anything:

```rust
impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        3
    }
}
```

Here is an example where the third lifetime elision rule applies:

```rust
impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please: {}", announcement);
        self.part
    }
}
```

### Static lifetime
Denotes that the affected reference can live for the entire duration of the 
program. All string literals have the `'static` lifetime:

```rust
let s: &'static str = "I have a static lifetime.";
```

The text of this string is stored directly in the program's binary, which is 
always available. If the compiler suggests the use of `'static`, fix dangling 
issues and lifetime mismatch first.

### Generic type parameters, trait bounds, lifetimes together
To specify generic type parameters, trait bounds and lifetimes all in one 
function:

```rust
use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```