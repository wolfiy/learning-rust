# Chapter 4
Ownership is Rust's most unique feature and enables the language to make memory 
safety guarentees without the need of a garbage collector.

## Definition
*Ownership* is a set of rules that govern how a Rust program manages memory. In
Rust, this is done using a system of ownership with a set of rules that the 
compiler checks. These will not slow the program down while it is running.

### The stack and the heap
In Rust, whether a value is on the stack or the heap (parts of memory available) 
affects how the language behaves and why some decisions have to be made.

> **The stack** stores values in the order it gets them and removes values in 
the opposite order. This is refered to as *last in, first out*

> **The heap** is less organized; when data is put on the heap, a certain amount 
of space is requested. The memory allocator then finds a big enough spot in the 
heap, marks it as being in use and returns a *pointer* which is the address of 
that location. Since the pointer is of a known and fixed size, it can be stored 
on the stack. To get the data, you must follow the pointer.

Pushing to the stack is obviously faster than allocating on the heap (no need to 
search for a place). Same thing for accessing data. When a function is called, 
the values and the local variables get pushed onto the stack, and when the 
function is over, those values get popped off the stack.

Ownership addresses issues such as what parts to put on the heap, minimizing the 
duplicates on the heap and cleaning up usued data.

### Variable scope
A scope is the range within a program for which an item is valid.
```rust
// 's' is not valid here since it has not been yet declared.
fn main() {
    // 's' is valid from this point forward.
    let s = "hello";
}
// The scopoe is over; 's' is no longer valid.
```

### The string type
The types covered in chapter 3 are of known size and can be stored on the stack.
More complex types like `String` are a great example of data stored on the heap.

*String literals* is where a string value is hardcoded into the program. They 
are convenient but not suitable for every situation since

- They are not immutable;
- Not every string is of known size.

Rust has a second, immutable, string type which stores data on the heap:
```rust
let s = String::from("hello");
```
This kind of string can be mutated:
```rust
let mut s = String::from("hello");

// Append a literal to a String.
s.push_str(", world");

// Prints "hello, world".
println!("{}", s);
```

### Memory and allocation
With the `String` type, in order to support a mutable, growable piece of text, 
we need to allocate an amount of memory on the heap which is unkown at compile 
time. This means:

- Memory must be requested from the allocator at runtime (`String::from`);
- We need a way to return the memory when we are done with the `String`.

In Rust, the memory is automatically returned once the variable that owns it 
goes out of scope by calling the `drop` function.

### Variables and data interaction with move
Multiple variables can interact with the same data in different ways.
```rust
// Both values are equal to 5, and both integer get pushed onto the stack.
let x = 5;
let y = x;
```

A String is made up of a pointer, a length and a capacity. This group of data 
is stored on the stack
```rust
// The second line does not make a copy of the first.
let s1 = String::from("hello");
let s2 = s1;
```

On the right, the memory on the heap holds the content.

<img src="https://doc.rust-lang.org/book/img/trpl04-01.svg" width="20%">

The length is how much memory in bytes the content of the String is using. The 
capacity is the total amount of memory (bytes) recieved from the allocator. When 
assigning `s1` to `s2` *only* the pointer gets copied:

<img src="https://doc.rust-lang.org/book/img/trpl04-02.svg" width="20%">


When `s2` and `s1` go out of scope, both will try ot free the same memory 
(*double free* error). To ensure memory safety, Rust considers `s1` no longer
valid after the line `let s2 = s1;`. This is called a *move* and deep copies 
are never done.

```rust
// This code will not compile, since 's1' is an invalid reference.
let s1 = String::from("hello");
let s2 = s1;
println!("{}, world!", s1);
```

### Variables and data interacting with clone
In case a deep copy is needed, we can use a common method called `clone`.
```rust
let s1 = String::from("hello");
let s2 = s1.clone();

// The first string was not invalidated; the code compiles.
println!("s1 = {}, s2 = {}", s1, s2);
```

### Stack only data: copy
Types such as integers with known sizes at compile time are stored entirely on 
the stack, so copies are actually very quick to make (i.e. no difference between 
deep and shallow copies) thus `clone` is useless.

The `Copy` trait can be placed on types that are stored on the stack to prevent 
variables from moving and forcing them to be copied, making them still valid 
after assignment to another variable. This cannot be done on a type which has 
implemented the `Drop` trait.

### Ownership and functions
Functions behave similarly as assignments.
```rust
fn main() {
    // 's' comes into scope.
    let s = String::from("hello");  // s comes into scope

    // The value of 's' moves into the function and so is no longer valid here.
    takes_ownership(s);

    // 'x' comes into scope.
    let x = 5;

    // 'x' would move into the function, but i32 is Copy so it's okay to still
    // use 'x' afterwards.
    makes_copy(x);

} 
// Here, x goes out of scope, then s. But because s's value was moved, nothing
// special happens.

// some_string comes into scope.
fn takes_ownership(some_string: String) {
    println!("{}", some_string);
} 
// 'some_string' goes out of scope and `drop` is callede; memory is freed.

// 'some_integer' comes into scope.
fn makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}
// 'some_integer' goes out of scope. Nothing special happens.
```

Trying to use `s` after the call to `take_ownership` would cause a compile-time 
error.


### Return Values and Scope
Values returned by functions can also transfer ownership.

```rust
fn main() {
    let s1 = gives_ownership();         // gives_ownership moves its return
                                        // value into s1

    let s2 = String::from("hello");     // s2 comes into scope

    let s3 = takes_and_gives_back(s2);  // s2 is moved into
                                        // takes_and_gives_back, which also
                                        // moves its return value into s3
} 
// 's3' goes out of scope and is dropped, 's2' was moved so nothing happens.
// 's1' goes out of scope and is dropped.

// 'give_ownership' will move its return valu into the function that calls it.
fn gives_ownership() -> String {

    // 'some_string' comes into scope
    let some_string = String::from("yours");

    // 'some_string' is returned and moves out to the calling function.
    some_string
}

// This function takes a String and returns one. 'a_string' comes into scope.
fn takes_and_gives_back(a_string: String) -> String {

    // 'a_string' is returned and moves out to the calling function.
    a_string
}
```
The ownership of a variable always follows the same pattern: assigning a value 
to another variable moves it. When a variable that includes data on the heap 
goes out of scope, the value will be cleaned up by `drop` unless its been moved 
to another variable. Multiple values can be returned using a tuple.

To let a function use a value without taking its ownership, we use references.

## References and borrowing
To make references, use an `&` sign before the type. Take the following code:
```rust
// Using a reference instead of taking ownership.
fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

```
The issue with (4-5) is that we have to return the `String` to the calling 
function to be able to use it after the call to `calculate_length()`. Instead, 
we can provide a reference to the `String` value, which is like a pointer (i.e. 
is an address). The difference is that it guarantees to point to a valid value 
of a particular type for the life of that reference.

<img src="https://doc.rust-lang.org/book/img/trpl04-05.svg" width="35%">

When the reference stops being used, the value does not get dropped (since it 
does not own it). These can also be used in function signatures; this is called 
borrowing. Note that references are immutables.

### Mutable references
Here is an example:
```rust
fn main() {
    let mut s = String::from("hello");

    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```
> One big restriction when using mutable references: if you have a mutable 
reference to some value, you can have no other references to that value. This 
is to avoid *data races*. Curly brackets can be used to create a new scope and 
allow for multiple mutable references, just not simultaneously.

```rust
let mut s = String::from("hello");

let r1 = &s; // no problem
let r2 = &s; // no problem
let r3 = &mut s; // BIG PROBLEM

println!("{}, {}, and {}", r1, r2, r3);
```

However, no problem her:
```rust
let mut s = String::from("hello");

let r1 = &s; // no problem
let r2 = &s; // no problem
println!("{} and {}", r1, r2);
// Variables r1 and r2 will not be used after this point; their scope ended.

// The scopes of r1 & r2 and r3 don't overlap; no problem.
let r3 = &mut s;
println!("{}", r3);
```

### Dangling references
A *dangling pointer* refers to a location in memory that may have been given to 
something else. In Rust, the compiler prevents dangling references by ensuring 
that the data will not go out of the scope before the reference to the data 
does. The following code would not even compile:

```rust
fn main() {
    let reference_to_nothing = dangle();
}

// 'dangle()' returns a reference to a String.
fn dangle() -> &String {
    // 's' is a new String.
    let s = String::from("hello");

    // A reference to 's' is returned.
    &s
}
// Here 's' goes out of scope and is dropped. Its memory goes away. Danger!
```

The solution is to return the `String` directly:
```rust
fn no_dangle() -> String {
    let s = String::from("hello");
    s
}
```

### TL;DR
- At any given time, you can have either one mutable reference or any number of 
immutable references;
- References must be valid.

## The slice type
*Slices* allow a reference to a contigous sequence of elements in a collection 
rather thant the collection as a whole. Since it is a kind of reference, it 
does not have ownership.

> **On an example.** Write a function that takes a string of words separated by 
spaces and returns the first word of said string. If no space is found, return 
the whole string.

```rust
// We don't want ownership. Will return index of the end os the word.
fn first_word(s: &String) -> usize {
    // Since we'll iterate through the String element by element, we convert
    // our String to an array of bytes.
    let bytes = s.as_bytes();

    // 'iter()' returns each element in a collection, 'enumerate()' wraps the
    // result and return each element as part of a tuple instead.
    // The first element of the tuple is the index, the second a reference to
    // the element.
    // & is used since 'enumerate()' gives a reference.
    for (i, &item) in bytes.iter().enumerate() {
        // Checking for a space using the byte literal syntax.
        if item == b' ' {
            return i;
        }
    }

    s.len();
}
```
However: we are returning a `usize` on its own, which is only meaningful in the 
context of the `&String`; there is no guarentee it will still be valid in the 
future.
```rust
fn main() {
    let mut s = String::from("hello world");
    
    // word will get the value '5'.
    let word = first_word(&s);

    // Empties the String, making it equal to ''.
    s.clear();

    // 'word' still has the value '5' here, but there is no more string that we 
    // could meaningfully use the value '5' with. 'word' is now invalid.
}
```
Using the value '5' stored in `word` would be a bug because the contents of 's' 
have changed since we saved '5' in `word`. Having to worry about the index in 
`word` getting out of sync with the data in `s` is tedious and error prone.

Slices solve such issues.

### String slices
A *string slice* is a reference to part of a String.

```rust
let s = String::from("hello world");

// References to a postion of 's'. With [starting_index..ending_index].
// Here 'world' contains a pointer to the byte at index 6 of 's' with a length 
// value of '5'.
let hello = &s[0..5];
let world = &s[6..11];

// Other notations for indexes:
// [..2] = [0..2], [4..len] = [4..], [..] = [0..len]
```

<img src="https://doc.rust-lang.org/book/img/trpl04-06.svg" width="20%">

> **Note.** Slices must be made at valid UTF-8 boundaries (not mid-way to some 
multi-byte character).

Back to the previous example. The code would become
```rust
//'&str' means string slice. This version of 'first_word' returns a single 
// value that is tied to the underlaying data.
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    // Same way of getting the index.
    for (i, &item) in bytes.iter().enumerate() {
        // Once a space is found, return the whole string.
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
```
This would also prevent the bug.

### String Literals as Slices
In `s = "Hello world!";`, the type of `s` is `&str`; it's a slice pointing at 
a specific point of the binary. This is also the reason as to why string 
literals are immutable references.

### String Slices as Parameters
We can improve the example even further by changing the signature of 
`first_word` to either one of the following:

```rust
fn first_word(s: &String) -> &str { }
fn first_word(s: &str) -> &str { }
```

With the latter allowing to use the function with both strings and string 
slices.

```rust
fn main() {
    let my_string = String::from("hello world");

    // `first_word` works on slices of `String`s, whether partial or whole
    let word = first_word(&my_string[0..6]);
    let word = first_word(&my_string[..]);
    // `first_word` also works on references to `String`s, which are equivalent
    // to whole slices of `String`s
    let word = first_word(&my_string);

    let my_string_literal = "hello world";

    // `first_word` works on slices of string literals, whether partial or whole
    let word = first_word(&my_string_literal[0..6]);
    let word = first_word(&my_string_literal[..]);

    // Because string literals *are* string slices already,
    // this works too, without the slice syntax!
    let word = first_word(my_string_literal);
}
```

### Other Slices
There is also a more general type of slices.

```rust
// 'slice' has the type '&[i32]' and works the same way as string slices.
let a = [1, 2, 3, 4, 5];
let slice = &a[1..3];
assert_eq!(slice, &[2, 3]);
```