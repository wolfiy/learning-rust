# Chapter 5
A *struct(ure)* is a custom data type that allows the packaging of multiple 
related values to make up a meaningful group (similar to attributes in OOP).

## Defining and instantiating structs
A `struct` can hold different types of variables, and each piece of data gets a 
name. This makes `struct` more flexible than tuples, as you do not have to rely 
on an the order to access data.

```rust
// Definition of a struct.
struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

// Instantiation of a struc. The order of keys don't matter.
fn main() {
    let user1 = User {
        active: true,
        username: String::from("someusername123");
        email: String::from("someone@example.com");
        sign_in_count: 1,
    };

    // Getting a value.
    let s: String = user1.email;

    // Changing a value (needs to be mutable).
    user1.email = String::from("anotheremail@example.com");
}

// Builder returning a 'User' instance.
fn build_user(email: String, username: String) -> User {
    User {
        active: true,
        username: username,
        email: email,
        sign_in_count: 1,
    }
}
```

### Using the field init shorthand
When parameter names and struct field names are exactly the same, a shorthand 
can be used:

```rust
fn build_user(email: String, username: String) -> user {
    User {
        active: true,
        username,
        email,
        sign_in_count: 1,
    }
}
```

### Creating instances from other instances with struct update syntax
To create a new instance that only differ in some values.

```rust
// The ..user1 line has to come last.
fn main() {
    let user2 = User {
        email: String::from("another@example.com");
        ..user1
    };
}
```

> **Note.** The struct update syntax uses `=` like assignments because it moves 
the data (cf. chapter 4), meaning that here `user1` as a whole cannot be used 
anymore. If `user2` was only using the `active` and `sign_in_count` values from 
`user1`, it would still be valid afterwards (since they would be copied).

### Using tuples structs without named fields to create different types
They have the added meaning the struct name provides but no names associated to 
their fields; they just have the types.

```rust
// Definition.
struct Color(i32, i32, i32):
struct Point(i32, i32, i32);

fn main() {
    // Different types since different struct. A struct = a new type.
    // 'name.index' to access an individual value.
    let black = Color(0, 0, 0);
    let apoint = Point(1, 2, 3);
}
```

### Unit-like structs without any fields
These are structs withtout any fields. Can be useful when we need to implement 
a trait on some type but do not have any data to store.

```rust
// Definition and usage.
struct AlwaysEqual;

fn main() {
    let subject = AlwaysEqual;
}
```

## Method syntax
Similar to functions but defined within the context of a struct / enum / trait 
object. Their first parameter is *always* `self`, which represent the instance 
of the calling struct.

### Defining methods
On an example:

```rust
// 
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

// The area function has been changed to accept a Rectangle instance as a 
// parameter, making a method defined on the Rectangle struct.
impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}
```

To define the function in the context of a `Rectangle`, we need an `impl` block 
for those. We then move the function inside of that block and make the first 
parameter `self` in both the signature and body.

In the signature, we use `&slef` Within an `impl` block, the type `Self` is an 
alias for whatever type the block is for. If we'd want to change the instance 
that we've called, we would use `&mut self` as the first parameter.

Methods are useful for organization; with them all the things we can do with an 
instance of a type is put in one `impl` block. Note that we can choose to give 
a method the same name as one of the `struct`'s fields:

```rust
impl Rectangle {
    // Returns true if the width is greater than 0. Rust knows that if 
    // parentheses were used, we are calling a method. 
    fn width(&self) -> bool {
        self.width > 0
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    if rect1.width() {
        println!("The rectangle has a nonzero width; it is {}", rect1.width);
    }
}
```